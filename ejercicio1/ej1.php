<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Pedidos</title>
</head>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<body>
<?php

define(
    "PRECIOS",
    [
        "Jamon-y-Queso" => 5,
        "Napolitana" => 4.5,
        "Mozarella" => 6
    ]
);

function writeFile($nameFile, $arrayData)
{
    $fp = fopen(dirname(__FILE__) . "/" . $nameFile, "a");
    fwrite($fp, $arrayData);
}


function getPizzas($array)
{
    $pizzas = [];
    foreach ($array as $key => $value) {
        if (gettype($key) == "string") {
            $pizza = substr($key, 0, 6);
            if ($pizza == "pizza-") {
                $name = str_replace("-", " ", substr($key, 6));
                $pizzas[$name] = [$array["number" . substr($key, 6)], PRECIOS[substr($key, 6)]];
            }
        }
    }
    return $pizzas;
}

function calc($array)
{
    return $array[0] * $array[1] + $array[1] * 0.008;
}

function getPrice($array)
{
    $price = array_sum(array_map("calc", $array));
    return $price;
}

function writeFactura($array)
{
    $pizzas = getPizzas($_GET);
    if (getPrice($pizzas) != 0) {
        $infoName = "Nombre: " . $array["name"];
        $infoDir = "Direccion: " . $array["dir"];
        $total = "Total: " . getPrice($pizzas);

        $factura = $infoName . PHP_EOL;
        $factura .= $infoDir . PHP_EOL;

        echo "<h2> FACTURA </h2>";
        echo "<p> ${infoName} </p>";
        echo "<p>${infoDir}</p>";

        foreach ($pizzas as $key => $value) {
            echo "<p> " . $key . " x " . $value[0] . ": " . $value[1] . "&euro;</p>";
            $factura .= $key . " x " . $value[0] . ": " . $value[1] . PHP_EOL;
        }
        echo "---------------------------------------";
        echo "<p> ${total} </p>";
        $factura .= "${total}" . PHP_EOL;
        $factura .= date("d-m-Y G:i:s") . PHP_EOL;
        $factura .= "===================" . PHP_EOL;
        writeFile("pedidos.txt", $factura);
        chmod("pedidos.txt", 0777);
    } else {
        echo "<p>No ha elegido ninguna pizza</p>";
    }
}

if (isset($_GET['submit'])) {
    writeFactura($_GET);
    $direction = $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
    echo "<a href='http://${direction}'> Ir atŕas </a>";
} else {
    echo '<div class="container-fluid">
            <form id="form" class="" action="ej1.php" method="get">
                <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="name" value="" required>
                </div>
                <div class="form-group">
                    <label for="">Dirección</label>
                    <input type="text" name="dir" value="" required>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="pizza-Jamon-y-Queso">
                    <label for="">Jamón y Queso</label>
                    <input type="number" name="numberJamon-y-Queso" min="1" value="1">
                    <label for="">Cantidad</label>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="pizza-Napolitana">
                    <label for="">Napolitana</label>
                    <input type="number" name="numberNapolitana" min="1" value="1">
                    <label for="">Cantidad</label>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="pizza-Mozarella">
                    <label for="">Mozarella</label>
                    <input type="number" name="numberMozarella" min="1" value="1">
                    <label for="">Cantidad</label>
                </div>
                <input type="submit" name="submit" value="Enviar">
            </form>
        </div>';
}
?>
</div>
</body>

</html>
