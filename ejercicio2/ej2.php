<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Ej2</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>

</head>
<body>
<form class="form" action="ej2.php" method="get">
    <div class="form-group">
        <label for="">Nombre</label>
        <input type="text" name="name" value="" required>
    </div>
    <div class="form-group">
        <label for="">Tramo salarial</label>
        <select class="" name="tramo">
            <option value="1">1-1000</option>
            <option value="2">1001-3000</option>
            <option value="3">>3000</option>
        </select>
        <div class="form-group">
            <label for="">Ingresos</label>
            <input type="number" name="ingresos" value="1000" min="1">
        </div>
        <input type="submit" name="submit" value="Enviar">
    </div>

</form>

<?php
if (isset($_GET['submit'])) {

    function calcula($iva, $salario, $nombre)
    {
        $impuestos = ($iva * $salario) / 100;
        $impuestos = number_format($impuestos, 3);
        echo "<p> ${nombre}: Debe pagar ${impuestos} &euro; de impuestos. </p>";
    }

    $error = false;
    $iva = 0;
    switch ($_GET["tramo"]) {
        case "1":
            if (!($_GET['ingresos'] > 1000)) {
                $iva = 10;
            }
            break;
        case "2":
            if (!($_GET['ingresos'] > 3000 || $_GET['ingresos'] < 1001)) {
                $iva = 15;
            }
            break;

        case '3':
            if (!($_GET['ingresos'] < 3001)) {
                $iva = 20;
            }
            break;

        default:
            break;
    }

    if ($iva != 0) {
        calcula($iva, $_GET['ingresos'], $_GET['name']);
    } else {
        echo "<p>Ha cometido un error en sus datos.</p>";
    }
}
?>
</body>
</html>
