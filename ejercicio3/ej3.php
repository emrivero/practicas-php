<?php

function checkVisits($number)
{
    if (($number % 10) == 0) {
        $timesWin = $number / 10;
        $prize = ($timesWin - 1) * 0.05 + 1;
        echo "Enhorabuena! Esta es su visita ${number}. Recibirá un premio de ${prize} &euro;.";
    }
}

function sumVisits($filename)
{
    $number = 1;
    if (!file_exists($filename)) {
        $fp = fopen($filename, "wb");

    } else {
        $fp = fopen($filename, "rb");
        $number = fread($fp, filesize($filename));
        $number = intval($number) + 1;
        fclose($fp);
        $fp = fopen($filename, "wb");
        checkVisits($number);
        $number = strval($number);
    }
    fwrite($fp, $number);
    fclose($fp);
    chmod($filename, 0777);

}

sumVisits('counter.txt');


