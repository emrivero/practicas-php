<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
</head>
<body>
<form class="" enctype="multipart/form-data" action="process_file.php" method="post">
    <div class="form-group">
        <label for="">Nombre</label>
        <input type="text" name="name" value="" required>
    </div>
    <div class="form-group">
        <label for="">Apellidos</label>
        <input type="text" name="subname" value="" required>
    </div>
    <div class="form-group">
        <label for="">Dirección</label>
        <input type="text" name="addr" value="" required>
    </div>
    <div class="form-group">
        <input type="file" name="user_file" value="">
        <input type="submit" name="" value="Subir">
    </div>
</form>

</body>
</html>