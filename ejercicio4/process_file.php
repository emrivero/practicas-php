<?php

$namefolder = 'subidas';
$name = $namefolder . '_' . random_int(0, 31104000000) . '.txt';
$filepath = dirname(__FILE__) . DIRECTORY_SEPARATOR . $namefolder;
$filename = $filepath . DIRECTORY_SEPARATOR . $name;

if (isset($_FILES['user_file'])) {
    $dataFile = $_FILES['user_file'];
    $fileSize = $_FILES['user_file']['size'];
    $file = $dataFile['tmp_name'];
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $maxSize = 1048576;
    if (!empty($file)) {
        $mimeType = $finfo->file($file);
        if ($mimeType == "text/plain") {

            $array = [
                PHP_EOL,
                "Nombre: " . $_POST['name'] . PHP_EOL,
                "Apellidos: " . $_POST['subname'] . PHP_EOL,
                "Dirección: " . $_POST['addr'] . PHP_EOL,
                "Fecha: " . date("d-m-Y") . PHP_EOL,
                "Hora: " . date("H:i:s") . PHP_EOL,
            ];

            if ($fileSize <= $maxSize) {
                createFile($filepath, $file, $filename, $array);
                showInfo($namefolder, $name, $filename, $fileSize, $file);
            } else {
                echo "<p> Error: El fichero ocupa más de 1 MB</p>";
            }
        } else {
            echo "<p> Error: El fichero tiene que ser de tipo texto</p>";
        }
    } else {
        echo "<p> Error: Fichero vacio</p>";
    }
}

// Function that creates a file
function createFile($path, $file, $filename, $array)
{
    if (!is_dir($path)) {
        mkdir($path);
    }
    $fp = fopen($file, "rb");
    $fread = fread($fp, filesize($file) + 1);
    foreach ($array as $value) {
        $fread .= $value;
    }
    $fsave = fopen($filename, "wb");
    fwrite($fsave, $fread);
    fclose($fsave);
    fclose($fp);
    chmod($filename, 0777);
    chmod($path, 0777);
}

function showInfo($folder, $file, $realpath, $filesize)
{
    $content = file_get_contents($realpath);
    echo '<p>Ruta relativa: ', $folder, DIRECTORY_SEPARATOR, $file, '</p>';
    echo '<p>Ruta absoluta: ', $realpath, '</p>';
    echo '<p>Tamaño: ', number_format($filesize / 1024, 2), ' kb', '</p>';
    echo '<p>Contenido: ', $content, '</p>';
}


?>
