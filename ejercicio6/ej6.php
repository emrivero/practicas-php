<?php
if (isset($_POST['submit'])) {
  $texto;
  if ($_POST['case'] == "upper") {
    $texto = strtoupper($_POST['texto']);
  } else {
    $texto = strtolower($_POST['texto']);
  }

  $bold = "";
  if (isset($_POST['negrita'])) {
    $bold = "bold";
  }

  $color = "black";
  if (isset($_POST['color'])) {
    $color = $_POST['color'];
  }
  $bgcolor = "white";
  if (isset($_POST['bgcolor'])) {
    $bgcolor = $_POST['bgcolor'];
  }

  $subrayado = "";
  if (isset($_POST['subrayado'])) {
    $subrayado = "underline";
  }

  $cursiva = "";
  if (isset($_POST['cursiva'])) {
    $cursiva = "italic";
  }

  echo "<h2> Texto Formateado </h2>";
  echo "<p>Texto: <span style='color:${color}; background-color:${bgcolor}; font-weight:${bold}; font-style:${cursiva}; text-decoration:${subrayado}'> " . $texto . " </span></p>";
  echo "<a href='${_SERVER['PHP_SELF']}'>Volver </a>";
} else {
  include 'index.php';
}
