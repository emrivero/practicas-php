<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
    <link rel="stylesheet" href="main.css">

</head>
<body>
<form action="ej6.php" method="post">
    <h3>Variant Font</h3>
    <div class="input-group">
        <label for="">Negrita</label>
        <input type="checkbox" name="negrita">
    </div>
    <div class="input-group">
        <label for="">Cursiva</label>
        <input type="checkbox" name="cursiva">
    </div>
    <div class="input-group">
        <label for="">Subrayado</label>
        <input type="checkbox" name="subrayado">
    </div>
    <h3>Mayusculas/Minusculas</h3>
    <div class="input-group">
        <label for="">Mayusculas</label>
        <input type="radio" name="case" value="upper" checked>
    </div>
    <div class="input-group">
        <label for="">Minusculas</label>
        <input type="radio" name="case" value="lower">
    </div>
    <h3>Color de fuente</h3>
    <div class="input-group">
        <label for="">Azul</label>
        <input type="radio" name="color" value="blue">
    </div>
    <div class="input-group">
        <label for="">Verde</label>
        <input type="radio" name="color" value="green">
    </div>
    <div class="input-group">
        <label for="">Rojo</label>
        <input type="radio" name="color" value="red">
    </div>
    <h3>Color de fondo</h3>
    <div class="input-group">
        <label for="">Gris</label>
        <input type="radio" name="bgcolor" value="grey">
    </div>
    <div class="input-group">
        <label for="">Amarillo</label>
        <input type="radio" name="bgcolor" value="yellow">
    </div>
    <div class="input-group">
        <label for="">Negro</label>
        <input type="radio" name="bgcolor" value="black">
    </div>
    <div class="input-group">
        <label for="">Texto</label>
        <br/>
        <textarea name="texto" rows="8" cols="80" placeholder="Escriba aquí..."></textarea>
    </div>
    <input type="submit" name="submit" value="Enviar">
    </div>
</form>
</body>

</html>
