<?php
  function isDiv($numero, $div) {
    $isDiv = $numero % $div == 0 ? "<p>${numero} es divisible entre ${div}</p>": "<p>${numero} no es divisible entre ${div}</p>";
    echo $isDiv;
  }

if (isset($_POST['submit'])) {
  if (is_numeric($_POST['numero'])) {
    for ($i = 0; $i< 12; $i++) {
      if (isset($_POST["${i}"])) {
          isDiv($_POST['numero'], strval($_POST["${i}"]));
      }
    }
  } else {
    echo "<p> El dato introducido no es numerico</p>";
  }
  echo "<a href=${_SERVER['PHP_SELF']}>Volver</a>";
} else {
  include 'form.html';
}
 ?>
