<?php
session_start();
if (isset($_POST['submit-login'])) {
    require 'config.php';

    $dbm = new mysqli($host, $user, $pass, "Usuario");

    if (!($stmt = $dbm->prepare("SELECT user, pass FROM Dato WHERE user=?"))) {
        echo $dbm->errno . " " . $dbm->error;
    }
    $stmt->bind_param("s", $_POST['user']);
    $stmt->execute();
    $stmt->bind_result($dataUser, $passUser);
    $stmt->fetch();

    if ($dataUser) {
        if ($_POST['pass'] == $passUser) {
            $_SESSION['user'] = $dataUser;
            header("location: http://${_SERVER['SERVER_NAME']}/ejercicio8/ej8.php");
        } else {
            echo "Error en autenticación";
            header("refresh:4; url=ej8.php");
        }
    } else {
        header("location: http://${_SERVER['SERVER_NAME']}/ejercicio8/register.php");
    }
} else {
    header("location: http://${_SERVER['SERVER_NAME']}/ejercicio8/ej8.php");
}

?>
