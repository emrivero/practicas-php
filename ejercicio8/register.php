<?php
session_start();
if (isset($_SESSION['user'])) {
    header("location: http://${_SERVER['SERVER_NAME']}/ejercicio8/ej8.php");
} else {
    if ($_POST['submit']) {
        require 'config.php';

        $dbm = new mysqli($host, $user, $pass, "Usuario");
        if (!($stmt = $dbm->prepare("INSERT INTO Dato (user, pass) VALUES(?,?)"))) {
            echo $dbm->errno . " " . $dbm->error;
        }

        $stmt->bind_param("ss", $_POST['user'], $_POST['pass']);
        $stmt->execute();
        header("location: http://${_SERVER['SERVER_NAME']}/ejercicio8/ej8.php");
    }
}
include 'register.html';
?>
