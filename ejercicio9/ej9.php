<?php
session_start();
require_once 'functions.php';

if (!isset($_SESSION['user'])) {
    header("location: http://${_SERVER['SERVER_NAME']}/ejercicio9/login.php");
} else {
    redirect();
}

