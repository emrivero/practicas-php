<?php
function exist_user($user, $pass, $config)
{
    $dbm = new mysqli($config['host'], $config['user'], $config['pass'], $config['dbname']);
    $stmt = $dbm->prepare("SELECT user, pass FROM Usuario WHERE user=? and pass=?");
    if (!$stmt) {
        echo $dbm->errno . " " . $dbm->error;
    }
    $stmt->bind_param("ss", $user, $pass);
    $stmt->execute();
    $stmt->bind_result($dataUser, $passUser);
    $stmt->fetch();
    return !empty($dataUser);
}

function exist_username($user, $config)
{
    $dbm = new mysqli($config['host'], $config['user'], $config['pass'], $config['dbname']);
    $stmt = $dbm->prepare("SELECT user FROM Usuario WHERE user=?");
    if (!$stmt) {
        echo $dbm->errno . " " . $dbm->error;
    }
    $stmt->bind_param("s", $user);
    $stmt->execute();
    $stmt->bind_result($dataUser);
    $stmt->fetch();
    return !empty($dataUser);
}

function insert_user($user, $pass, $config)
{
    $dbm = new mysqli($config['host'], $config['user'], $config['pass'], $config['dbname']);
    $stmt = $dbm->prepare("INSERT INTO Usuario (user, pass) VALUES(?,?)");
    if (!$stmt) {
        echo $dbm->errno . " " . $dbm->error;
    }
    $stmt->bind_param("ss", $user, $pass);
    $stmt->execute();
    $stmt->fetch();
}

function get_score($user, $config)
{
    $select = "SELECT score FROM Usuario WHERE user  = ?";
    $dbm = new mysqli($config['host'], $config['user'], $config['pass'], $config['dbname']);
    $stmt = $dbm->prepare($select);
    $stmt->bind_param("s", $user);
    $stmt->execute();
    $stmt->bind_result($result_score);
    $stmt->fetch();
    return $result_score;
}

function update_score($user, $score, $time, $config)
{
    if (get_score($user, $config) < $score) {
        $dbm = new mysqli($config['host'], $config['user'], $config['pass'], $config['dbname']);
        $stmt = $dbm->prepare("UPDATE Usuario SET score = ?, time = ? WHERE user = ?");
        if (!$stmt) {
            echo $dbm->errno . " " . $dbm->error;
        }
        $stmt->bind_param("iis", $score, $time, $user);
        $stmt->execute();
        $stmt->fetch();
    }
}

function get_best_scores($config)
{
    $select = "SELECT user, score FROM Usuario ORDER BY score DESC LIMIT 5";
    $dbm = new mysqli($config['host'], $config['user'], $config['pass'], $config['dbname']);
    $stmt = $dbm->prepare($select);
    $stmt->execute();
    $stmt->bind_result($result_user, $result_score);
    $results = [];
    while ($stmt->fetch()) {
        array_push($results, ['user' => $result_user, 'score' => $result_score]);
    }
    return $results;
}

function redirect()
{

    if (!isset($_SESSION['ask']) || !isset($_SESSION['user'])) {
        if (basename($_SERVER['PHP_SELF']) != "login.php") {
            header("location: http://${_SERVER['SERVER_NAME']}/ejercicio9/login.php");
        }
    } else {
        $ask = $_SESSION['ask'];
        if (basename($_SERVER['PHP_SELF']) != "pregunta_{$ask}.php") {
            header("location: http://${_SERVER['SERVER_NAME']}/ejercicio9/preguntas/pregunta_{$ask}.php");
        }
    }

}

function calculate_score($answers, $correct_answers)
{
    $score = 0;
    foreach ($answers as $key => $value) {
        if ($correct_answers[$key] == $value) {
            $score += 25;
        }
    }
    return $score;
}

function calculate_time_score($time, $score)
{
    $result = 0;
    if ($time < 40 && $score == 100) {
        $result = 100 - ((5 * $time) / 2);
    }
    return $result;
}