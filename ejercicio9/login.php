<?php
session_start();
require_once 'config.php';
require_once 'functions.php';
redirect();
if (isset($_POST['user']) && isset($_POST['pass'])) {
    if (exist_user($_POST['user'], $_POST['pass'], $config)) {
        $_SESSION['user'] = $_POST['user'];
        $_SESSION['ask'] = 1;
        header("location: http://${_SERVER['SERVER_NAME']}/ejercicio9/preguntas/pregunta_1.php");
    } else {
        header("location: http://${_SERVER['SERVER_NAME']}/ejercicio9/signin.php");
    }
} else {
    echo '<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title>Concurso</title>
    <link rel="stylesheet" href="css/main.css">
  </head>

  <body>
   
    <div class="nav">
       <h1>Concurso</h1>
      <form action="login.php" method="post">
        <input type="text" name="user" class="username" placeholder="Username" required/>
        <input type="pass" name="pass" class="username" placeholder="Password" required/>
        <input type="submit" name="submit-login" value="Empieza a jugar" class="button" alt="" /></br>
      </form>
    </div>
  </body>

</html>';
}