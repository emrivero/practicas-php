<?php
session_start();
require_once '../functions.php';
redirect();

$_SESSION['start'] = time();
echo '<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title>Concurso</title>
    <link rel="stylesheet" href="../css/pregunta.css">
  </head>

  <body>
   
    <div class="nav">
       <h1>¿Como se llama este personaje?</h1>
       <img src="../img/pregunta.jpg" alt="imagen" class="img">
      <form action="pregunta_2.php" method="post" class="form-quiz">
        <div class="group">
            <input type="radio" name="ask1" class="username" value="1" />
            <label>Howard Wollowitz</label>
        </div>
        <div class="group">
            <input type="radio" name="ask1" class="username" value="2"/>
            <label>Sheldon Cooper</label>
        </div>
        <div class="group">
            <input type="radio" name="ask1" class="username" value="3"/>
            <label>Leonard Hofstader</label>
        </div>
        <div class="group">
            <input type="radio" name="ask1" class="username" value="4"/>
            <label>Rajesh Koothrappali</label>
        </div>
        <div class="group">
        <input type="hidden" name="ask" value="2"> 
            <input type="submit" name="submit-login" value="Responde" class="button" alt="" />
        </div>
      </form>
    </div>
  </body>

</html>';