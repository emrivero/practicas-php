<?php
session_start();
require_once 'config.php';
require_once 'functions.php';

if (isset($_POST['user_reg']) && isset($_POST['pass_reg'])) {
    if (!empty($_POST['user_reg']) && !empty($_POST['pass_reg']))
        if (!exist_username($_POST['user_reg'], $config)) {
            insert_user($_POST['user_reg'], $_POST['pass_reg'], $config);
            header("location: http://${_SERVER['SERVER_NAME']}/ejercicio9/login.php");
        } else {
            $_SESSION['exist_user'] = true;
            header("location: http://${_SERVER['SERVER_NAME']}/ejercicio9/signin.php");
        }
} else {
    header("location: http://${_SERVER['SERVER_NAME']}/ejercicio9/signin.php");
}
