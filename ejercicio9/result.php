<?php
session_start();
require_once 'functions.php';
require_once 'config.php';
if (isset($_POST['ask4'])) {

    $_SESSION['ask4'] = $_POST['ask4'];
    if (!isset($_SESSION['end'])) {
        $_SESSION['end'] = time();
    }
    $total_time = ($_SESSION['end'] - $_SESSION['start']);

    $answers = [
        "ask1" => $_SESSION["ask1"],
        "ask2" => $_SESSION["ask2"],
        "ask3" => $_SESSION["ask3"],
        "ask4" => $_SESSION["ask4"],
    ];

    $correct_answers = [
        "ask1" => "3",
        "ask2" => "3",
        "ask3" => "4",
        "ask4" => "2",
    ];

    $score = calculate_score($answers, $correct_answers);
    $score += calculate_time_score($total_time, $score);
    $score = intval($score);
    update_score($_SESSION['user'], $score, $total_time, $config);
    echo "Tiempo: {$total_time} segundos";
    echo '<h2>', $_SESSION['user'], ', tu puntación ha sido: ', $score, '</h2>';
    echo '<h3>', 'TOP 5', '</h3>';
    echo '<ul>';
    $best_scores = get_best_scores($config);
    foreach ($best_scores as $item) {
        echo '<li>', $item['user'], ': ', $item['score'], ' puntos', '</li>';
    }
    echo '</ul>';
    echo "<a href='logout.php'> Salir</a>";
} else {
    redirect();
}
